﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JsonTblsController : ControllerBase
    {
        private readonly MachineTestContext _context;

        public JsonTblsController(MachineTestContext context)
        {
            _context = context;
        }

        // GET: api/JsonTbls
        [HttpGet]
        public async Task<ActionResult<IEnumerable<JsonTbl>>> GetJsonTbl()
        {
            return await _context.JsonTbl.ToListAsync();
        }

        // GET: api/JsonTbls/5
        [HttpGet("{id}")]
        public async Task<ActionResult<JsonTbl>> GetJsonTbl(int id)
        {
            var jsonTbl = await _context.JsonTbl.FindAsync(id);

            if (jsonTbl == null)
            {
                return NotFound();
            }

            return jsonTbl;
        }

        // PUT: api/JsonTbls/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutJsonTbl(int id, JsonTbl jsonTbl)
        {
            if (id != jsonTbl.JsonId)
            {
                return BadRequest();
            }

            _context.Entry(jsonTbl).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JsonTblExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/JsonTbls
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<JsonTbl>> PostJsonTbl(JsonTbl jsonTbl)
        {
            _context.JsonTbl.Add(jsonTbl);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetJsonTbl", new { id = jsonTbl.JsonId }, jsonTbl);
        }

        // DELETE: api/JsonTbls/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<JsonTbl>> DeleteJsonTbl(int id)
        {
            var jsonTbl = await _context.JsonTbl.FindAsync(id);
            if (jsonTbl == null)
            {
                return NotFound();
            }

            _context.JsonTbl.Remove(jsonTbl);
            await _context.SaveChangesAsync();

            return jsonTbl;
        }

        private bool JsonTblExists(int id)
        {
            return _context.JsonTbl.Any(e => e.JsonId == id);
        }
    }
}
