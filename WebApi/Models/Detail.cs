﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Detail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Hobbies { get; set; }
        public string Gender { get; set; }
        public string Place { get; set; }
    }
}
