﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class JsonTbl
    {
        public int JsonId { get; set; }
        public string JsonData { get; set; }
    }
}
