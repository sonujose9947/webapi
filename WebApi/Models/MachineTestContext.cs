﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApi.Models
{
    public partial class MachineTestContext : DbContext
    {
        public MachineTestContext()
        {
        }

        public MachineTestContext(DbContextOptions<MachineTestContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Detail> Detail { get; set; }
        public virtual DbSet<JsonTbl> JsonTbl { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
               optionsBuilder.UseSqlServer("Server=DESKTOP-C0ECT77; Database=MachineTest; Integrated Security=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Detail>(entity =>
            {
                entity.Property(e => e.Gender)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Hobbies).HasMaxLength(100);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Place)
                    .HasMaxLength(20)
                    .IsFixedLength();
            });

            modelBuilder.Entity<JsonTbl>(entity =>
            {
                entity.HasKey(e => e.JsonId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
